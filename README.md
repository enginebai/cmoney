## Questions
目標 base API: `https://jsonplaceholder.typicode.com`, end point: `/photos`
做出兩個頁面,第一個頁面只需要有一個 Button 換場到下一個頁面。
第二個頁面要把 API 的內容呈現，需要呈現的項目有三個,分別是 “id”, “title”, “thumbnailUrl”，Id 和 title 是 String，thumbnailUrl 是圖案的網址,要在 cell 內的 imageView 中呈現每一行放四個格子
UI 要能自動適應各尺寸大小的 iPhone

實作請使用 java 或者是 kotlin 兩者擇一

禁止使用第三方套件
除了上述指定 UI 條件之外,你也可以自行優化沒指定的 UI

完成後，請將原始碼寄回給我們，感謝

## Screenshow
![main](./arts/main.png)
![detail1](./arts/detail1.png)
![detail2](./arts/detail2.png)