package com.enginebai.cmoney

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.enginebai.cmoney.ui.DetailFragment
import com.enginebai.cmoney.ui.MainFragment

class MainActivity : AppCompatActivity(), MainFragment.OnPageClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction()
            .add(R.id.fragmentContainer, MainFragment().apply { clickListener = this@MainActivity })
            .commit()
    }

    override fun onDetailClicked() {
        supportFragmentManager.beginTransaction()
            .add(R.id.fragmentContainer, DetailFragment())
            .addToBackStack(DetailFragment::class.java.simpleName)
            .commit()
    }
}
