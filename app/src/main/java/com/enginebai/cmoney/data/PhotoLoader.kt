package com.enginebai.cmoney.data

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future

interface PhotoLoadListener {
    fun onPhotoFetched(url: String, bitmap: Bitmap?)
}

object PhotoLoader {

    private val executorService: ExecutorService by lazy { Executors.newFixedThreadPool(10) }
    private val tasks: MutableMap<String, Future<*>> = mutableMapOf()

    fun loadPhoto(url: String, listener: PhotoLoadListener) {
        val task = executorService.submit(PhotoLoadJob.newJob(url, listener))
        tasks[url] = task
    }

    fun stopLoadJob(url: String) {
        tasks[url]?.cancel(true)
        tasks.remove(url)
    }

    class PhotoLoadJob private constructor(
        private val url: String,
        private val listener: PhotoLoadListener
    ) : Runnable {
        override fun run() {
            Log.d("enginebai", "Fetch photo $url")
            if (url.isBlank()) return
            var connection: HttpURLConnection? = null
            try {
                connection =
                    (URL(url).openConnection() as HttpURLConnection).apply {
                        requestMethod = "GET"
                        connectTimeout = 1000
                        defaultUseCaches = true
                        doInput = true
                        setRequestProperty(
                            "User-Agent",
                            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36"
                        )
                    }
                connection.connect()
                Log.d("enginebai", "Get response code ${connection.responseCode} from $url")
                if (connection.responseCode == 200) {
                    val bitmap = BitmapFactory.decodeStream(connection.inputStream)
                    listener.onPhotoFetched(url, bitmap)
                }
            } catch (e: IOException) {
                Log.w("enginebai", e.message ?: "Unknown IOException")
            } finally {
                connection?.inputStream?.close()
                connection?.disconnect()
            }
        }

        companion object {
            fun newJob(url: String, listener: PhotoLoadListener) = PhotoLoadJob(url, listener)
        }
    }
}