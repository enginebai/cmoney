package com.enginebai.cmoney.data

data class PhotoModel(
    val id: String,
    val title: String? = null,
    val thumbnailUrl: String? = null
)