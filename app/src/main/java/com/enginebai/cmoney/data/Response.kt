package com.enginebai.cmoney.data

class Response<T>(val isSuccessfull: Boolean, val data: T? = null, val errorMsg: String? = null)