package com.enginebai.cmoney.data

import com.enginebai.cmoney.BuildConfig
import org.json.JSONArray
import org.json.JSONTokener
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

interface TypicodeApiService {
    fun fetchPhotos(): Response<List<PhotoModel>>
}

class TypicodeApiServiceImpl : TypicodeApiService {
    override fun fetchPhotos(): Response<List<PhotoModel>> {
        var connection: HttpsURLConnection? = null
        val url = URL("${BuildConfig.BASE_API}/photos")
        return try {
            connection = (url.openConnection() as? HttpsURLConnection)
            connection?.run {
                requestMethod = "GET"
                connectTimeout = 1000
                defaultUseCaches = true
                doInput = true

                connect()
                if (responseCode != HttpURLConnection.HTTP_OK) {
                    Response(false, errorMsg = "HTTP error code $responseCode")
                } else {
                    inputStream?.let { stream ->
                        val reader = BufferedReader(InputStreamReader(stream))
                        val content = StringBuilder()
                        reader.use { r ->
                            var line = r.readLine()
                            while (line != null) {
                                content.append(line)
                                line = r.readLine()
                            }
                        }
                        val jsonArray = JSONArray(JSONTokener(content.toString()))
                        val photoList = mutableListOf<PhotoModel>()
                        for (i in 0 until jsonArray.length()) {
                            val jsonObj = jsonArray.getJSONObject(i)
                            photoList.add(
                                PhotoModel(
                                    id = jsonObj.getString("id"),
                                    title = jsonObj.getString("title"),
                                    thumbnailUrl = jsonObj.getString("thumbnailUrl")
                                )
                            )
                        }
                        Response(true, photoList.toList())
                    }
                }
            } ?: throw IOException("The connection is null.")
        } finally {
            connection?.inputStream?.close()
            connection?.disconnect()
        }
    }

}