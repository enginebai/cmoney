package com.enginebai.cmoney.data

import com.enginebai.cmoney.di.Injection

interface TypicodeRepo {
    fun fetchPhotos(): Response<List<PhotoModel>>
}

class TypicodeRepoImpl : TypicodeRepo {

    private val api: TypicodeApiService by lazy { Injection.provideTypicodeApi() }
    override fun fetchPhotos(): Response<List<PhotoModel>> = api.fetchPhotos()
}