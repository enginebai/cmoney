package com.enginebai.cmoney.di

import com.enginebai.cmoney.data.TypicodeApiService
import com.enginebai.cmoney.data.TypicodeApiServiceImpl
import com.enginebai.cmoney.data.TypicodeRepo
import com.enginebai.cmoney.data.TypicodeRepoImpl

object Injection {

    private val typicodeRepo: TypicodeRepo by lazy { TypicodeRepoImpl() }
    private val typicodeApi: TypicodeApiService by lazy { TypicodeApiServiceImpl() }

    fun provideTypicodeRepo(): TypicodeRepo = typicodeRepo
    fun provideTypicodeApi(): TypicodeApiService = typicodeApi
}