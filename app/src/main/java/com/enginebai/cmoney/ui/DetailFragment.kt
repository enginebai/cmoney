package com.enginebai.cmoney.ui

import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.enginebai.cmoney.R
import com.enginebai.cmoney.base.BaseFragment
import com.enginebai.cmoney.data.PhotoModel
import com.enginebai.cmoney.data.Response
import kotlinx.android.synthetic.main.fragment_detail.*
import java.io.IOException

interface FetchCallback {
    fun performFetch(): Response<List<PhotoModel>>?
    fun onFetched(list: List<PhotoModel>)
    fun onError(errMsg: String?)
}

class DetailFragment : BaseFragment(), FetchCallback {

    private lateinit var viewModel: DetailViewModel
    private var fetchTask: FetchTask? = null
    private val photoAdapter: PhotoAdapter by lazy { PhotoAdapter() }

    override fun getLayoutId() = R.layout.fragment_detail

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.run { viewModel = ViewModelProvider(this).get(DetailViewModel::class.java) }
        initList()
        val task = FetchTask(this)
        task.execute()
    }

    private fun initList() {
        with(list) {
            layoutManager = GridLayoutManager(list.context, 4)
            adapter = photoAdapter
        }
    }

    override fun onDestroyView() {
        fetchTask?.cancel(true)
        fetchTask = null
        super.onDestroyView()
    }

    @WorkerThread
    override fun performFetch(): Response<List<PhotoModel>>? {
        return viewModel.fetchPhotos()
    }

    @MainThread
    override fun onFetched(list: List<PhotoModel>) {
        photoAdapter.photoList = list
        photoAdapter.notifyDataSetChanged()
    }

    @MainThread
    override fun onError(errMsg: String?) {
        activity?.run {
            Toast.makeText(this, errMsg ?: "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private class FetchTask(private val callback: FetchCallback? = null) :
        AsyncTask<Unit, Unit, Response<List<PhotoModel>>?>() {
        override fun doInBackground(vararg params: Unit?): Response<List<PhotoModel>>? {
            return try {
                callback?.performFetch()
            } catch (e: IOException) {
                Response(false, errorMsg = e.message)
            }
        }

        override fun onPostExecute(result: Response<List<PhotoModel>>?) {
            super.onPostExecute(result)
            result?.data?.run {
                callback?.onFetched(this)
            } ?: kotlin.run { callback?.onError(result?.errorMsg) }
        }
    }

}