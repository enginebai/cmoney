package com.enginebai.cmoney.ui

import androidx.lifecycle.ViewModel
import com.enginebai.cmoney.data.PhotoModel
import com.enginebai.cmoney.data.Response
import com.enginebai.cmoney.data.TypicodeRepo
import com.enginebai.cmoney.di.Injection

class DetailViewModel : ViewModel() {
    private val repo: TypicodeRepo by lazy { Injection.provideTypicodeRepo() }
    fun fetchPhotos(): Response<List<PhotoModel>>? = repo.fetchPhotos()
}