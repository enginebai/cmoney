package com.enginebai.cmoney.ui

import android.os.Bundle
import android.view.View
import com.enginebai.cmoney.R
import com.enginebai.cmoney.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : BaseFragment() {

    interface OnPageClickListener {
        fun onDetailClicked()
    }

    var clickListener: OnPageClickListener? = null

    override fun getLayoutId() = R.layout.fragment_main

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonRequestApi.setOnClickListener { clickListener?.onDetailClicked() }
    }
}