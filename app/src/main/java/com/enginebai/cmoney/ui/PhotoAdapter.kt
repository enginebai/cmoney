package com.enginebai.cmoney.ui

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enginebai.cmoney.R
import com.enginebai.cmoney.data.PhotoLoadListener
import com.enginebai.cmoney.data.PhotoLoader
import com.enginebai.cmoney.data.PhotoModel

class PhotoAdapter : RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder>() {

    var photoList: List<PhotoModel> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false)
        return PhotoViewHolder(view)
    }

    override fun getItemCount(): Int = photoList.size

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val photo = photoList.getOrNull(position)
        photo?.run {
            holder.data = this
            holder.textId.text = this.id
            holder.textTitle.text = this.title
            holder.imagePhoto.setImageBitmap(null)
            PhotoLoader.loadPhoto(this.thumbnailUrl ?: "", object : PhotoLoadListener {
                override fun onPhotoFetched(url: String, bitmap: Bitmap?) {
                    bitmap?.run {
                        if (true == holder.data?.thumbnailUrl?.equals(url)) {
                            holder.imagePhoto.setImageBitmap(this)
                        }
                    }
                }
            })
        }
    }

    override fun onViewDetachedFromWindow(holder: PhotoViewHolder) {
        super.onViewDetachedFromWindow(holder)
        PhotoLoader.stopLoadJob(holder.data?.thumbnailUrl ?: "")
    }

    class PhotoViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val imagePhoto: ImageView by lazy { view.findViewById<ImageView>(R.id.imagePhoto) }
        val textId: TextView by lazy { view.findViewById<TextView>(R.id.textId) }
        val textTitle: TextView by lazy { view.findViewById<TextView>(R.id.textTitle) }
        var data: PhotoModel? = null
    }
}

